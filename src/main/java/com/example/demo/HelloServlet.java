package com.example.demo;

import java.io.*;
import java.text.DecimalFormat;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private TemperatureConverter temperatureConverter;

    public void init() {
        temperatureConverter = new TemperatureConverter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperate Conversion";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celcius " +
                request.getParameter("temp") + "\n" +
                "  <P>In Fahrenheit: " +
                (new DecimalFormat("0.00").format(temperatureConverter.convertTemperature(request.getParameter("temp")))) +
                "</BODY></HTML>");

    }

    public void destroy() {
    }
}