package com.example.demo;

public class TemperatureConverter {
    public double convertTemperature(String tempInCelcius) {
        double temp = Double.parseDouble(tempInCelcius);
        return (temp * 1.8 + 32);
    }
}
